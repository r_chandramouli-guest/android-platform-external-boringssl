Source: android-platform-external-boringssl
Section: libs
Priority: optional
Maintainer: Android Tools Maintainers <android-tools-devel@lists.alioth.debian.org>
Uploaders: Kai-Chung Yan <seamlikok@gmail.com>
Standards-Version: 4.1.4
Build-Depends: debhelper (>= 11),
               dh-exec,
               golang-go,
               perl (>= 5.6.1)
Vcs-Git: https://salsa.debian.org/android-tools-team/android-platform-external-boringssl.git
Vcs-Browser: https://salsa.debian.org/android-tools-team/android-platform-external-boringssl
Homepage: https://android.googlesource.com/platform/external/boringssl

Package: android-libboringssl
Architecture: any
Depends: ${misc:Depends}, ${shlibs:Depends}
Description: Google's internal fork of OpenSSL for the Android SDK
 The Android SDK builds against a static version of BoringSSL,
 Google's internal fork of OpenSSL.  This package should never be used
 for anything but Android SDK packages that already depend on it.
 .
 BoringSSL arose because Google used OpenSSL for many years in various ways and,
 over time, built up a large number of patches that were maintained while
 tracking upstream OpenSSL. As Google’s product portfolio became more complex,
 more copies of OpenSSL sprung up and the effort involved in maintaining all
 these patches in multiple places was growing steadily.
 .
 Currently BoringSSL is the SSL library in Chrome/Chromium, Android (but it's
 not part of the NDK) and a number of other apps/programs. The libraries are
 installed in a private directory and are not for general use.

Package: android-libboringssl-dev
Section: libdevel
Architecture: all
Depends: ${misc:Depends}
Description: Google's internal fork of OpenSSL for the Android SDK - devel
 The Android SDK builds against a static version of BoringSSL,
 Google's internal fork of OpenSSL.  This package should never be used
 for anything but Android SDK packages that already depend on it.
 .
 BoringSSL arose because Google used OpenSSL for many years in various ways and,
 over time, built up a large number of patches that were maintained while
 tracking upstream OpenSSL. As Google’s product portfolio became more complex,
 more copies of OpenSSL sprung up and the effort involved in maintaining all
 these patches in multiple places was growing steadily.
 .
 Currently BoringSSL is the SSL library in Chrome/Chromium, Android (but it's
 not part of the NDK) and a number of other apps/programs. The libraries are
 installed in a private directory and are not for general use.
 .
 This package contains the header files.
